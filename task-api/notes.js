/*
	Server Connect on MongoDB via Mongoose ODM

	1. Create The project folder called `task-api`
	2. Initialize an npm by running command:

		$ npm init
	3. Create the app entry point file:
		
		$ touch index.js
	4. Install the packages for express, nodemon, and mongoose db by running command:

		$ npm install express nodemon mongoose
	5. Set up the initial codes for creating a server 
	6. Open your MongoDB atlas account then create a new DB called `b125-tasks` with an initial collection called `tasks`
	7. Get a connection string on the MongoDB Atlas and update its password and the DB name:

	sample:
	mongodb+srv://aidanbigornia:<password>@cluster0.e3n78.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
*/