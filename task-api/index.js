const express = require("express");
const mongoose = require('mongoose'); // this code is to be used in our db connection and the create of our schema and model for our existing MongoDB atlas collection
const app = express(); // creating a server through the use of app variable
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true})); // allows to accept data form from the client

mongoose.connect(`mongodb+srv://aidanbigornia:Big0rnia@cluster0.e3n78.mongodb.net/b125-tasks?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	}
).then(() => { //if mongoose succeeded on the connection, then we will run the statement
	console.log(`Successfully Connected to Database.`)
}).catch((error) =>{ //handles error when the mongoose failed to connect on our mongodb atlas database

	console.log(error);
});
// is a way to connect our mongodb atlas db to our server
// paste inside the connect method the connection string we copied from the mongodb
// remember to replace the password and db name
// Due to updates made by MongoDB Atlas developers, the default connection string is being flagged as an error, to skip that error or warning in the future, we will use the useNewUrlParser and useUnifiedTopology objects inside the mongoose.connect(,)
/*--------------------------------------------------------------------------*/

// Schema - gives a structure of what kind of record/document we are going to contain on our database
// Schema() method - determines the structure of the documents to be written in the database
// Schema acts as blueprint to our data
// We used the Schema() constructor of the mongoose dependency to create a new schema object for our task collection
// the "new" keyword, creates a new schema
const taskSchema = new mongoose.Schema({
	// Define the fields with their corresponding data type
	// for task, it needs a field called 'name' and 'status'
	// The field name has a data type of `String`
	// The field name has a data type of `Boolean` with a default value of `false`
	name: String,
	status: {
		type: Boolean,
		// Default values are the predefined values for a field if we don't put any value
		default: false
	}
});

/*Models to perform the CRUD operations for our defined collections with their corresponding schema*/
// The Task variable will contain the model for our task collection that and shall perform the CRUD operations.
// The first parameter of the mongoose.model method indicates the collection in where to store the data. Take Note: the collection name must be written in singular form and first letter of the name must be in Uppercase.
// The second parameter is used to specify the schema/blueprint of the document that will be stored on the task collection
const Task = mongoose.model(`Task`, taskSchema);

/*
	Mini Activity Instruction:

	1. Create a Schema for users collection with below fields and data types

		firstName - string
		lastName - string
		username - string
		passowrd - string
*/
const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String
});

const User = mongoose.model(`User`, userSchema);

app.post('/register', (req, res)=>{

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		username: req.body.username,
		password: req.body.password
	});

	newUser.save((error,savedUser) =>{
		if(error){
			console.log(error);
		} else {
			res.send(`new user saved! ${savedUser}`)
		}

	});
});
/*
	Business Logic - Todo List Application
	- CRUD Operation for our Tasks collection
*/
// insert new task
app.post('/add-task', (req, res)=>{
	// call model for our tasks collection
	// create an instance(document) of the task model and save it to our database
	// creating a new Task with a task name "PM Break" through the use of the Task Model for lines 78-80
	let newTask = new Task({
		name: req.body.name
	});
	// line 82 is telling our server to save newTask will now be saved as a new document to our Task Collection to our database.
	// save() method - saves a new document to our db
	// on our callback, it will receive 2 parameters, the error and the saved document
	// error value shall contain the error whenever there is an error encountered while we are saving our document.
	// saveTask shall contain the new saved document from the db once the saving process is successful
	newTask.save((error, savedTask) => {
		if(error){
			console.log(error);
		} else {
			res.send(`New task saved! ${savedTask}`);
		}
	});
});

/*
	Mini activity - perform Create operation for our users collection 
	1. Create a route with an endpoint `/register` that shall do the following:
		a. Receives a request body with field and values for firstName, lastName, and password from the client
		b. Use the request body to create a instance of new user model
		c. Save the new user model and once the user is saved, send a response back to the client the user detail

		Register these users:
		Peter Parker, spidey, spiderman2021
*/

// RETRIEVE
app.get(`/retrieve-tasks`, (req, res)=>{
	// find({}) will retrieve all the documents from the tasks collections
	// the error on the callback will handle the errors encountered while retrieve the records 
	// the records on the callback will handle the raw data from the database
	Task.find({}, (error, records) =>{
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});
// retrieve tasks that are done, means status == true
app.get(`/retrieve-tasks-done`, (req,res)=>{
	// the Task Model will return all the tasks that has a status equal to true
	Task.find({status: true}, (error,records )=>{
		if(error){
			console.log(error);
		} else {
			res.send(records)
		}
	})
})


// Update operation
app.put(`/complete-task/:taskId`, (req, res)=>{
	// res.send({ urlParams: req.params.taskId});
	// 1. Find the specific record using its ID
	// 2. Update it

	// findByIdAndUpdate()
	// let taskId = "Where should we get the task ID?";
	let taskId = req.params.taskId;
	// url parameters - these are the values defined on the URLs
	// to get the url parameters - req.params.<paramsName>
	// :taskId - is a way to indicate that we are going to receive a url parameter, these are what we call a 'wildcard'

	Task.findByIdAndUpdate(taskId, { status:true},(error, updatedTask )=>{
		if (error){
			console.log(error);
		} else {
			res.send(`Task completed Successfully!`)
		};

	});
}); 

// DELETE Operation
app.delete(`/delete-task/:taskId`, (req,res)=>{
	// findByIdAndDelete() - finds the specific record using its Id and delete it.
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send(`Task Deleted!`)
		}
	})
})

app.listen(PORT, (req,res) => console.log(`Server is running at port ${PORT}`));